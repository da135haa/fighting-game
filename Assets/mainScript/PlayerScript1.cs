﻿using UnityEngine;
using System.Collections;

public class PlayerScript1 : MonoBehaviour
{
    static int hashSpeed = Animator.StringToHash("Speed");
    static int hashFallSpeed = Animator.StringToHash("FallSpeed");
    static int hashGroundDistance = Animator.StringToHash("GroundDistance");
    static int hashIsCrouch = Animator.StringToHash("IsCrouch");
    static int hashAttack1 = Animator.StringToHash("Attack1");
    static int hashAttack2 = Animator.StringToHash("Attack2");
    static int hashAttack3 = Animator.StringToHash("Attack3");


    static int hashDamage = Animator.StringToHash("Damage");
    static int hashIsDead = Animator.StringToHash("IsDead");

    public GameObject attack;

    [SerializeField]
    private float characterHeightOffset = 0.2f;
    [SerializeField]
    LayerMask groundMask;

    [SerializeField, HideInInspector]
    Animator animator;
    [SerializeField, HideInInspector]
    SpriteRenderer spriteRenderer;
    [SerializeField, HideInInspector]
    Rigidbody2D rig2d;

    bool lifeTag = true;
    //0可以被打 1CD 2 準備完成
    public int player1 = 0;
    private bool dead = true;

    //AI系統
    public GameObject enemy;
    private bool aIUp = false;
    private bool aIDown = false;
    private bool aILeft = false;
    private bool aIRight = false;
    private int aIAtk = 0;



    void Awake()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rig2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        aiGo();
       if (lifeTag)
        {
            //目前面相哪邊
            float axis = Input.GetAxisRaw("HorizontalPlayer1");
            //是不是在地板上
            bool isDown = Input.GetAxisRaw("VerticalPlayer1") < 0;


            //右左
            if (Input.GetKey(KeyCode.D) || aIRight)
            {
                if (aIRight)
                {
                    transform.position = new Vector3(transform.position.x + 0.01f, transform.position.y, transform.position.z);
                    aIRight = false;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x + 0.03f, transform.position.y, transform.position.z);
                }



                axis = 1;
                attack.transform.position = new Vector2(transform.position.x + 0.265f, attack.transform.position.y);

            }

            if (Input.GetKey(KeyCode.A) || aILeft)
            {
                if (aILeft)
                {
                    transform.position = new Vector3(transform.position.x - 0.01f, transform.position.y, transform.position.z);
                    aILeft = false;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x - 0.03f, transform.position.y, transform.position.z);
                }


                axis = -1;

                attack.transform.position = new Vector2(transform.position.x - 0.265f, attack.transform.position.y);

            }

            //跳躍
            if (Input.GetKeyDown(KeyCode.W) || aIUp)
            {
                aIUp = false;
                rig2d.velocity = new Vector2(rig2d.velocity.x, 5);
            }

            var distanceFromGround = Physics2D.Raycast(transform.position, Vector3.down, 1, groundMask);

            // update animator parameters
            animator.SetBool(hashIsCrouch, isDown);
            animator.SetFloat(hashGroundDistance, distanceFromGround.distance == 0 ? 99 : distanceFromGround.distance - characterHeightOffset);
            animator.SetFloat(hashFallSpeed, rig2d.velocity.y);

            animator.SetFloat(hashSpeed, Mathf.Abs(axis));



            //3種攻擊
            if (Input.GetKeyDown(KeyCode.J) || aIAtk == 1) { animator.SetTrigger(hashAttack1); aIAtk = 0; }
            if (Input.GetKeyDown(KeyCode.K) || aIAtk == 2) { animator.SetTrigger(hashAttack2); aIAtk = 0; }
            if (Input.GetKeyDown(KeyCode.L) || aIAtk == 3) { animator.SetTrigger(hashAttack3); aIAtk = 0; }

            AnimatorStateInfo stateinfo = animator.GetCurrentAnimatorStateInfo(0);
            //如果正在播放walk动画.
            if (stateinfo.IsName("Attack1") || stateinfo.IsName("Attack2") || stateinfo.IsName("SpecialAttack"))
            {
                attack.SetActive(true);
                attack.transform.position = new Vector2(attack.transform.position.x + 0.0001f, attack.transform.position.y);
                attack.transform.position = new Vector2(attack.transform.position.x - 0.0001f, attack.transform.position.y);
            }
            else
            {
                attack.SetActive(false);
            }


            // flip sprite
            if (axis != 0)
                spriteRenderer.flipX = axis < 0;

            //被攻擊時
            if (player1 == 1)
            {
                player1 = 2;
                animator.SetTrigger(hashDamage);
                StartCoroutine("cd");
            }
        }
        //沒血就掛了
        if (HeroInvincible.player1HP < 1)
        {
           
            if(dead)
            {
                animator.SetTrigger(hashIsDead);
                //不會被打
                player1 = 2;
                //死亡特效
                animator.Play("Holger_Damage_down");
                dead = false;
                //不能操控
                lifeTag = false;
                //攻擊關閉
                attack.SetActive(false);
                HeroInvincible.aiFlag = false;
            }
                    
           
        }

    }
    IEnumerator cd()
    {
        yield return new WaitForSeconds(0.5f);
        player1 = 0;
    }

    //放在Update中
    public void aiGo()
    {
        //先判斷對方在哪邊
        //判斷對方離自己多遠,夠進就開始攻擊
        //無聊可以跳個幾下
        //無聊可以蹲下
        float enemyX = enemy.transform.position.x;
        float myX = transform.position.x;
        float totalX = enemyX - myX;


        //+-0.2
        if (totalX > 0)
        {
            if (totalX > 0.3)
            {
                aIRight = true;
            }
            else
            {
                aIAtk = Random.Range(1, 4);

            }
        }
        else
        {
            if (totalX < -0.3)
            {
                aILeft = true;
            }
            else
            {
                aIAtk = Random.Range(1, 4);
            }
        }

        if (Random.Range(1, 150) == 1)
        {
            aIUp = true;
        }
        if (Random.Range(1, 20) == 1)
        {
            aIDown = true;
        }
    }
}
