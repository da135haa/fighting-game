﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackgroundScript : MonoBehaviour {

    //傷害判定和時間皆由這裡控制

    public Text timeText;
    public double gameTime = 30;

    

    // Use this for initialization
    void Start () {
     //   Instantiate(Resources.Load("Unitychan_Battle(Sword)"), new Vector2(0.187f, -1.266f), Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
          
        if(HeroInvincible.aiFlag)
        {
            timeText.text = (gameTime -= Time.deltaTime).ToString("#0.00");

            if (HeroInvincible.totleAI < 6)
            {
                float x = Random.Range(-3.0f, 3.0f);
                float y = 2;
                Instantiate(Resources.Load("Holger_BattleAI"), new Vector2(x, y), Quaternion.identity);
                HeroInvincible.totleAI++;
            }
        }
       
	}
}
