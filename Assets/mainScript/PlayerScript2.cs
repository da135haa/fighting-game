﻿using UnityEngine;
using System.Collections;

public class PlayerScript2 : MonoBehaviour
{
	static int hashSpeed = Animator.StringToHash ("Speed");
	static int hashFallSpeed = Animator.StringToHash ("FallSpeed");
	static int hashGroundDistance = Animator.StringToHash ("GroundDistance");
	static int hashIsCrouch = Animator.StringToHash ("IsCrouch");
	static int hashAttack1 = Animator.StringToHash ("Attack1");
	static int hashAttack2 = Animator.StringToHash ("Attack2");
	static int hashAttack3 = Animator.StringToHash ("Attack3");


	static int hashDamage = Animator.StringToHash ("Damage");
	static int hashIsDead = Animator.StringToHash ("IsDead");

    public GameObject attack;
    private bool deadFlag = false;

	[SerializeField] private float characterHeightOffset = 0.2f;
	[SerializeField] LayerMask groundMask;

	[SerializeField, HideInInspector] Animator animator;
	[SerializeField, HideInInspector]SpriteRenderer spriteRenderer;
	[SerializeField, HideInInspector]Rigidbody2D rig2d;


	void Awake ()
	{
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		rig2d = GetComponent<Rigidbody2D> ();
    }

	void Update ()
	{
        if(!deadFlag)
        {
            float axis = Input.GetAxisRaw("HorizontalPlayer2");
            bool isDown = Input.GetAxisRaw("VerticalPlayer2") < 0;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                rig2d.velocity = new Vector2(rig2d.velocity.x, 5);
            }

            var distanceFromGround = Physics2D.Raycast(transform.position, Vector3.down, 1, groundMask);



            // update animator parameters
            animator.SetBool(hashIsCrouch, isDown);
            animator.SetFloat(hashGroundDistance, distanceFromGround.distance == 0 ? 99 : distanceFromGround.distance - characterHeightOffset);
            animator.SetFloat(hashFallSpeed, rig2d.velocity.y);
            animator.SetFloat(hashSpeed, Mathf.Abs(axis));

            //左右
            if (Input.GetKey(KeyCode.RightArrow))
            {

                attack.transform.position = new Vector2(transform.position.x + 0.265f, attack.transform.position.y);
                transform.position = new Vector3(transform.position.x + 0.03f, transform.position.y, transform.position.z);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                attack.transform.position = new Vector2(transform.position.x - 0.265f, attack.transform.position.y);
                transform.position = new Vector3(transform.position.x - 0.03f, transform.position.y, transform.position.z);
            }


            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                animator.SetTrigger(hashAttack1);
            }
            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                animator.SetTrigger(hashAttack2);
            }
            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                animator.SetTrigger(hashAttack3);
            }

            AnimatorStateInfo stateinfo = animator.GetCurrentAnimatorStateInfo(0);
            //如果正在播放walk动画.
            if (stateinfo.IsName("Attack1") || stateinfo.IsName("Attack2") || stateinfo.IsName("SpecialAttack"))
            {
                attack.SetActive(true);
                attack.transform.position = new Vector2(attack.transform.position.x + 0.0001f, attack.transform.position.y);
                attack.transform.position = new Vector2(attack.transform.position.x - 0.0001f, attack.transform.position.y);
            }
            else
            {
                attack.SetActive(false);
            }



            // flip sprite
            if (axis != 0)
                spriteRenderer.flipX = axis < 0;


            //被攻擊時
            if (HeroInvincible.player2 == 1)
            {
                HeroInvincible.player2 = 2;
                animator.SetTrigger(hashDamage);
                StartCoroutine("cd");
            }

            //沒血就掛了
            if (HeroInvincible.player2HP < 1)
            {
                HeroInvincible.player2 = 2;
                animator.SetTrigger(hashIsDead);
                deadFlag = true;
                attack.SetActive(false);
            }
        }		
    }
    IEnumerator cd()
    {
        yield return new WaitForSeconds(0.5f);
        HeroInvincible.player2 = 0;
    }
}
