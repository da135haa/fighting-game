﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player2Attack : MonoBehaviour
{

    public GameObject player1LifeObject;
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay2D(Collider2D collider2D)
    {
        if (collider2D.name.Equals("Toko_Battle"))
        {
            if (collider2D.GetComponent<PlayerScript1>().player1 == 0)
            {
                int damageHp = 40;

                HeroInvincible.player1HP -= damageHp;
                //更改RectTransform的width和height
                player1LifeObject.GetComponent<RectTransform>().sizeDelta = new Vector2(HeroInvincible.player1HP, 30);

                player1LifeObject.GetComponent<RectTransform>().transform.position = new Vector2(player1LifeObject.GetComponent<RectTransform>().transform.position.x+ damageHp/2, player1LifeObject.GetComponent<RectTransform>().transform.position.y);

                collider2D.GetComponent<PlayerScript1>().player1 = 1;
            }
        }
        else if(collider2D.name.Equals("Holger_BattleAI(Clone)"))
            {
            if (collider2D.GetComponent<AIScript>().player1 == 0)
            {
                collider2D.GetComponent<AIScript>().player1 = 1;
            }
                
        }
    }
}
