﻿using UnityEngine;
using System.Collections;

public class Player1Attack : MonoBehaviour {


    private GameObject player2LifeObject;
    // Use this for initialization
    void Start () {
        player2LifeObject = GameObject.Find("PlayerLife2");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D collider2D)
    {      

        if(collider2D.name.Equals("Unitychan_Battle(Sword)"))
        {
            int damageHp = 50;

            if (HeroInvincible.player2 == 0)
            {
                HeroInvincible.player2HP -= damageHp;
              

                //更改RectTransform的width和height
                player2LifeObject.GetComponent<RectTransform>().sizeDelta = new Vector2(HeroInvincible.player2HP, 30);

                player2LifeObject.GetComponent<RectTransform>().transform.position = new Vector2(player2LifeObject.GetComponent<RectTransform>().transform.position.x - damageHp/2, player2LifeObject.GetComponent<RectTransform>().transform.position.y);

                HeroInvincible.player2 = 1;
            }
        }
           
    }
}
